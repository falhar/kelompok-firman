const connection = require('../models/connection.js')

//make class transaksicontroller
class TransaksiController {
  async getAll(req, res) {
    try {
      var sql = "SELECT t.id as id_transaksi, b.nama as barang, p.nama as pelanggan, t.waktu, t.jumlah, t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id ORDER by t.id" // make an query varible

      // Run query
      connection.query(sql, function(err, result) {
        if (err) throw err; // If error

        // If success it will return JSON of result
        res.json({
          status: "success",
          data: result
        })
      });
    } catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }
  async getOne(req, res) {
    try {
      var sql = "SELECT t.id as id_transaksi, b.nama as barang, p.nama as pelanggan, t.waktu, t.jumlah, t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id where t.id=?" // make an query varible

      // Run query
      connection.query(sql, [req.params.id], function(err, result) {
        if (err) throw err; // If error

        // If success it will return JSON of result
        res.json({
          status: "success",
          data: result[0]
        })
      });
    } catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }
  async create(req, res) {
    try {
      var sql = 'SELECT harga FROM barang WHERE id=?'
      connection.query(sql, [req.body.id_barang], function(err, result) {
        if (err) {
          res.json({
            status: 'Error',
            error: err
          })
        }
        var total = result[0].harga * req.body.jumlah
        var sql1 = "insert into transaksi(id_barang, id_pelanggan, jumlah, total) values (?,?,?,?)" // make an query varible

        // Run query
        connection.query(sql1, [req.body.id_barang, req.body.id_pelanggan, req.body.jumlah, total], function(err, result) {
          if (err) throw err; // If error
          var sqlselect = 'SELECT transaksi.id as id_transaksi, barang.nama as barang, pelanggan.nama as pelanggan, transaksi.waktu, transaksi.jumlah, transaksi.total FROM transaksi JOIN barang on transaksi.id_barang=barang.id JOIN pelanggan on transaksi.id_pelanggan=pelanggan.id WHERE transaksi.id=?'
          // If success it will return JSON of result
          connection.query(sqlselect, [result.insertId], function(err, result) {
            if (err) {
              res.json({
                status: "Error",
                error: err
              });
            } // If error

            // If success it will return JSON of result
            res.json({
              status: "success add data",
              data: result[0]
            })
          });
        })
      });
    } catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }
  async update(req, res) {
    try {
      var sql = 'SELECT harga FROM barang WHERE id=?'
      connection.query(sql, [req.body.id_barang], function(err, result) {
        if (err) {
          res.json({
            status: 'Error',
            error: err
          })
        }
        var total = result[0].harga * req.body.jumlah
      connection.query(
        'UPDATE transaksi SET id_barang = ?, id_pelanggan = ?, jumlah = ?, total = ?, id = ? WHERE id = ?',
        [req.body.id_barang, req.body.id_pelanggan, req.body.jumlah, total, req.body.id, req.params.id],
        (err, result) => {
          if (err) throw err; // If error
          var sqlselect = 'SELECT transaksi.id as id_transaksi, barang.nama as barang, pelanggan.nama as pelanggan, transaksi.waktu, transaksi.jumlah, transaksi.total FROM transaksi JOIN barang on transaksi.id_barang=barang.id JOIN pelanggan on transaksi.id_pelanggan=pelanggan.id WHERE transaksi.id=?'
          // If success it will return JSON of result
          connection.query(sqlselect, [req.body.id], function(err, result) {
            if (err) {
              res.json({
                status: "Error",
                error: err
              });
            } // If error

            // If success it will return JSON of result
            res.json({
              status: "success add data",
              data: result[0]
            })
          });
        })
      });
    } catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }
  async delete(req, res) {
    try {
      var sql = 'DELETE FROM transaksi t WHERE id = ?'

      connection.query(
        sql,
        [req.params.id],
        (err, result) => {
          if (err) {
            res.json({
              status: "Error",
              error: err
            });
          } // If error

          // If success it will return JSON of result
          res.json({
            status: 'Success',
            data: result
          })
        }
      )
    } catch (err) {
      // If error will be send Error JSON
      res.json({
        status: "Error",
        error: err
      })
    }
  }
}
 

module.exports = new TransaksiController;
