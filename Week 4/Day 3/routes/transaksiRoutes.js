const express = require('express')
const router = express.Router()
const TransaksiController = require('../controllers/transaksiController.js')

router.get('/', TransaksiController.getAll)
router.get('/:id', TransaksiController.getOne)
router.post('/create', TransaksiController.create)
router.put('/update/:id', TransaksiController.update)
router.delete('/delete/:id', TransaksiController.delete)

module.exports = router
