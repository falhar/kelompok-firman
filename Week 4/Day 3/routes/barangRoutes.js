const express = require('express') // import express
const router = express.Router() //make router from app
const BarangController = require('../controllers/barangController.js') //import barang controller


router.get('/', BarangController.getAll) // if acessing localhost:3000/transaksi, it will do function getAll() in barangController class
router.get('/:id', BarangController.getOne) // if acessing localhost:3000/transaksi/:id, it will do function getOne() in barangController class
router.put('/update/:id', BarangController.update) // if acessing localhost:3000/update/:id, it will do function update() in barangController class
router.post('/create', BarangController.create) // if acessing localhost:3000/create, it will do function create() in transaksiController class
router.delete('/delete/:id', BarangController.delete) // if acessing localhost:3000/delete/:id, it will do function delete() in barangController class


module.exports = router; // Export router
