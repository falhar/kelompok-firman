const client = require('../models/connection.js')
const {
  ObjectId
} = require('mongodb')

class PemasokController {

  async getAll(req, res) {
    const penjualan = await client.db('penjualan')
    const pemasok = await penjualan.collection('pemasok')

    pemasok.find({}).toArray().then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async getOne(req, res) {
    const penjualan = await client.db('penjualan')
    const pemasok = await penjualan.collection('pemasok')

    pemasok.findOne({
      _id: new ObjectId(req.params.id)
    }).then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async create(req, res) {
    const penjualan = client.db('penjualan')
    const pemasok = penjualan.collection('pemasok')

    pemasok.insertOne({
      nama: req.body.nama,
    }).then(result => {
      res.json({
        status: "Success add new data",
        data: result.ops[0]
      })
    })
  }

  async update(req, res) {
    const penjualan = client.db('penjualan')
    const pemasok = penjualan.collection('pemasok')

    pemasok.updateOne({
      _id: new ObjectId(req.params.id)
    }, {
      $set: {
        nama: req.body.nama
      }
    }).then(() => {
      return pemasok.findOne({
        _id: new ObjectId(req.params.id)
      })
    }).then(result => {
      res.json({
        status: "Success update the data",
        data: result
      })
    })
  }

  async delete(req, res) {
    const penjualan = client.db('penjualan')
    const pemasok = penjualan.collection('pemasok')

    pemasok.deleteOne({
      _id: new ObjectId(req.params.id)
    }).then(result => {
      res.json({
        status: "Success delete data",
        data: null
      })
    })
  }
}

module.exports = new PemasokController
