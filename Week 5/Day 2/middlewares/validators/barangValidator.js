const client = require('../../models/connection.js');
const {ObjectId} = require('mongodb');
const {check, matchedData, validationResult, sanitize}=require('express-validator');

module.exports = {
  create: [
    check('nama').isString(),
    check('harga').isNumeric(),
    check('id_pemasok').custom(value=>{
      return client.db('penjualan').collection('pemasok').findOne({
        _id:new ObjectId(value)
      }).then(result=>{
        if (!result) {
          throw new Error('ID Pemasok tidak ada!');
        }
      })
    }),
    (req,res,next)=>{
      const errors=validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors:errors.mapped()
        })
      }
      next();
    }
  ],
  update:[
    check('id').custom(value=>{
      return client.db('penjualan').collection('barang').findOne({
        _id:new ObjectId(value)
      }).then(result=>{
        if (!result) {
          throw new Error('ID Barang tidak ada!')
        }
      })
    }),
    check('nama').isString(),
    check('harga').isNumeric(),
    check('id_pemasok').custom(value=>{
      return client.db('penjualan').collection('pemasok').findOne({
        _id:new ObjectId(value)
      }).then(result=>{
        if (!result) {
          throw new Error('ID Pemasok tidak ada!');
        }
      })
    }),
    (req,res,next)=>{
      const errors=validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors:errors.mapped()
        })
      }
      next();
    }
  ]
}
