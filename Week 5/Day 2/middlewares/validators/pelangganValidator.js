const client=require('../../models/connection.js');
const {ObjectId}=require('mongodb');
const {check,matchedData, sanitize, validationResult}=require('express-validator');

module.exports={
  create: [
    check('nama').isString(),
    (req,res,next) => {
      const errors=validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors:errors.mapped()
        })
      }
      next();
    }
  ],
  update:[
    check('nama').isString(),
    (req,res,next) => {
      const errors=validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors:errors.mapped()
        })
      }
      next();
    }
  ]
}
