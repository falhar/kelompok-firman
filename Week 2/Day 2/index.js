const readline=require("readline");
const rl=readline.createInterface({
    input:process.stdin,
    output:process.stdout
});
module.exports.rl=rl
const vball=require('./Ball.js');
const vcube=require('./kubus.js');
const vcone=require('./cone.js');
const lsegitiga=require('./Segitiga.js');


function isEmptyOrSpaces(str){
    return str === null || str.match(/^ *$/) !== null;
}
function optdum(opt) {
        if (opt==1) {
            vball.vball();
        } else if (opt==2) {
            vcube.rumuskubus();
        } else if (opt==3) {
            vcone.rumuscone();
        } else if (opt==4) {
            lsegitiga.rumussegitiga();
        } else if (opt==5) {
            rl.close();
        } else {
            console.log("Input only 1-5");
            option();
        }
}

console.log("Choose what you want to calculate");
console.log("=================================");
console.log("1. Ball");
console.log("2. Cube");
console.log("3. Cone");
console.log("4. Triangle");
console.log("5. Exit");
function option() {
    rl.question("Insert your choice : ", opt=>{
        if (!isNaN(opt)&&!isEmptyOrSpaces(opt)) {
        optdum(opt);
    } else {
        console.log("Input must be a number");
        option();
    }
})
}
option();