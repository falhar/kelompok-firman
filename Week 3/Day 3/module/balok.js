const bangunRuang = require('./bangunRuang.js')


class balok extends bangunRuang {

    constructor(Panjang, Lebar, Tinggi) {
        super('balok')
        this.Panjang = Panjang
        this.Lebar = Lebar
        this.Tinggi = Tinggi
    }

    menghitungBalok() {
        return this.Panjang * this.Lebar * this.Tinggi
    }

    // menghitungLuas() {
    //     return 2*(this.Panjang * this.Lebar * this.Tinggi)
    //   }


}

module.exports = balok