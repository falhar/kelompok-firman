const bangun=require("./bangun.js");

/* Make BangunDatar Abstract Class */
class BangunRuang extends bangun {

  // Make constructor with name variable/property
  constructor(name) {
    super('Bangun Ruang')
    if (this.constructor === BangunRuang) {
      throw new Error('This is abstract!')
    }
    this.name = name
  }

 // menghitungKeliling instance method
  menghitungVolume() {
    
    console.log('Volume bangun ruang');
  }
}
/* End BangunDatar Abstract Class */

module.exports = BangunRuang
