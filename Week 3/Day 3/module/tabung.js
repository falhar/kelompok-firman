// Import BangunRuang class
const bangunruang = require('./bangunRuang.js');

/* Make Lingkaran class that is parent of BangunDatar class (This is inheritance) */
class Tabung extends bangunruang {

  // Make constructor with radius, and height of lingkaran
  constructor(radius, height) {
    super('Tabung') // call the BangunDatar constructor, so this class will have all variable of the parent classs
    this.radius = radius // instance variable
    this.height = height // instance variable
  }

  // Overriding menghitungLuas from BangunDatar class
  menghitungLuas() {

    return 2*(Math.PI * Math.pow(this.radius, 2))+2 * Math.PI * this.radius*this.height
  }

  // Overriding menghitungKeliling from BangunDatar class
  menghitungVolume() {

    return Math.PI * Math.pow(this.radius, 2)*this.height
  }
}
/* End Persegi class */

module.exports = Tabung
