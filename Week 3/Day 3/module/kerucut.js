const bangunRuang = require('./bangunRuang.js')

class Kerucut extends bangunRuang {

  constructor(radius, tinggi) {
    super('Kerucut') // call the Bangunvolume constructor, so this class will have all variable of the parent classs
    this.radius = radius // instance variable
    this.tinggi = tinggi // instance variable

  }

  menghitungVolume() {
    
    return 3.14 * 1/3 * (this.radius ** 2) * this.tinggi 
  }


}

module.exports = Kerucut
