class Bangun {
  constructor(name) {
    if (this.constructor===Bangun) {
      throw new Error('This is abstract')
    }
    this.name=name
  }
  menghitungKeliling() {
    console.log(`Menghitung Keliling`);
  }
  menghitungLuas() {
    console.log(`Menghitung Luas`);
  }
  menghitungVolume() {
    console.log(`Menghitung Volume`);
  }
}

module.exports=Bangun
