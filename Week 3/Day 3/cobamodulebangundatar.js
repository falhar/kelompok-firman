// Import module.js
const importModule = require('./module/module.js')

// make object of importModule
const hitung = new importModule()

/* Start make calculate persegi */
let luasPersegi = hitung.menghitungLuasPersegi(20);
console.log(`Luas persegi adalah: ${luasPersegi}`);

let kelilingPersegi = hitung.menghitungKelilingPersegi(30)
console.log(`Keliling persegi adalah: ${kelilingPersegi}`);
/* End make calculate persegi */

/* Start make calculate persegi panjang */
let luasPersegiPanjang = hitung.menghitungLuasPersegiPanjang(10, 20)
console.log(`Luas persegi panjang adalah: ${luasPersegiPanjang}`);

let kelilingPersegiPanjang = hitung.menghitungKelilingPersegiPanjang(20, 30)
console.log(`Keliling persegi panjang adalah: ${kelilingPersegiPanjang}`);
/* End make calculate persegi panjang */

/* start make calculate lingkaran */
let luasLingkaran = hitung.menghitungLuasLingkaran(7)
console.log(`Luas lingkaran adalah ${luasLingkaran}`);

let kelilingLingkaran = hitung.menghitungKelilingLingkaran(14)
console.log(`Keliling lingkaran adalah: ${kelilingLingkaran}`);
/* End make calculate lingkaran */

// let luasTabung=hitung.menghitungLuasTabung(20,15)
// console.log(`Luas tabung adalah: ${luasTabung}`);

let volumeTabung=hitung.menghitungVolumeTabung(15,10)
console.log(`Volume tabung adalah: ${volumeTabung}`);

let volumeKubus = hitung.menghitungVolumeKubus(13)
console.log(`Volume kubus adalah: ${volumeKubus}`);

let volumeKerucut = hitung.menghitungVolumeKerucut(7,10)
console.log(`Volume kerucut adalah: ${volumeKerucut}`);

let balok = hitung.menghitungBalok(30,30,30)
console.log(`Volume balok adalah: ${balok}`);